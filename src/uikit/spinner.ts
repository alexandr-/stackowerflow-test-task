import styled from 'styled-components';

export const Spinner = styled.span<{$size?: number | string}>(({$size = 35}) => {
    const size = typeof $size === 'string' ? $size : $size + 'px';

    return `
        display: inline-block;
        width: ${size};
        height: ${size};
        
        &:after {
            content: "";
            display: block;
            width: ${size};
            height: ${size};
            border-radius: 50%;
            border: calc(${size} * 0.15) solid #fff;
            border-color: #1976d2 #1976d2 #1976d2 transparent;
            animation: spinner-rotate .8s linear infinite;
        }
        
        @keyframes spinner-rotate {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    `;
});