import styled from 'styled-components';

export const Button = styled.button`
  outline: 0;
  border: 0;
  border-radius: ${({theme}) => theme.borderRadius};
  background-color: #1976d2;
  color: #ffffff;
  cursor: pointer;
  transition: background-color ${({theme}) => theme.transition};

  &:hover {
    background-color: #166abe;
  }

  &:active {
    background-color: #135ea9;
  }
`;