import type {FC, ComponentProps} from 'react';
import {Backdrop, InlineContainer} from './styles';
import {Spinner} from '../spinner';

export const Loader: FC<ComponentProps<typeof Backdrop>> = (props) => {
    return (
        <Backdrop {...props}>
            <Spinner $size='6vmin'/>
        </Backdrop>
    );
}

export const InlineLoader: FC<ComponentProps<typeof InlineContainer>> = () => {
    return (
        <InlineContainer>
            <Spinner/>
        </InlineContainer>
    );
}