import type {FC, ComponentProps, ReactNode} from 'react';
import {InputWrap, InputView} from './styles';

interface InputProps extends ComponentProps<typeof InputView> {
    startIcon?: ReactNode;
    endIcon?: ReactNode;
}

export const Input: FC<InputProps> = ({startIcon, endIcon, ...rest}) => {
    return (
        <InputWrap>
            {startIcon}
            <InputView {...rest}/>
            {endIcon}
        </InputWrap>
    );
}