import styled from 'styled-components';

export const InputWrap = styled.label`
  width: 100%;
  display: flex;
  align-items: center;
  padding: 2px 10px;
  border: 2px solid #ccc;
  border-radius: ${({theme}) => theme.borderRadius};
  overflow: hidden;
  transition: border-color ${({theme}) => theme.transition};

  &:hover {
    border-color: #aaa;
  }

  &:focus-within {
    border-color: #1976d2;
  }
`;

export const InputView = styled.input`
  width: 100%;
  border: 0;
  outline: 0;
  padding: 5px 10px;
`;