import styled from 'styled-components';

export const Icon = styled.div<{$size?: number}>(({theme, onClick, onClickCapture, $size = 16}) => {
    const isClickable = onClick || onClickCapture;
    const clickableStyles = `
        &:hover {
            background-color: #ccc;
        }
        
        &:active {
            background-color: #bbb;
        }
    `;

    return `
        padding: 5px;
        width: ${$size}px;
        height: ${$size}px;
        font-size: ${$size}px;
        line-height: 1;
        text-align: center;
        border-radius: 50%;
        background-color: transparent;
        transition: background-color ${theme.transition};
        cursor: ${isClickable ? 'pointer' : ''};
        
        ${isClickable ? clickableStyles : ''}
    `;
});