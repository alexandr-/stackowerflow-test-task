import {useEffect, useState} from 'react';
import type {ID, Maybe} from '../../shared/types';
import {getQuestionById} from './api';
import {useSelector} from '../../shared/hooks';
import {selectQuestions} from './selectors';
import {compareId, validateId} from '../../shared/utils';
import type {Question} from './types';

export const useQuestionById = (id: ID) => {
    const {questions} = useSelector(selectQuestions);
    const [question, setQuestion] = useState<Maybe<Question>>();
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        if (question) setLoading(false);
    }, [question]);

    useEffect(() => {
        const cachedQuestion = questions.find(({question_id}) => compareId(id, question_id));

        if (cachedQuestion) return setQuestion(cachedQuestion);

        const abortController = new AbortController();
        getQuestionById(id, {signal: abortController.signal})
            .then(setQuestion)
            .catch(console.error);

        return () => abortController.abort();
        // eslint-disable-next-line
    }, [id]);

    if (!validateId(id)) throw new Error('Invalid question id');

    return {question, isLoading};
}