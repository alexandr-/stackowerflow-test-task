import {stackOverflowApi, RequestConfig} from '../../app';
import type {ID} from '../../shared/types';
import type {Question, QuestionsResponse, AnswersResponse} from './types';

export const getQuestions = async (search: string = '', config: RequestConfig = {}): Promise<QuestionsResponse> => {
    const {data} = await stackOverflowApi.get<QuestionsResponse>('/search/advanced', {
        ...config,
        params: {
            q: search,
            order: 'desc',
            sort: 'activity',
            filter: 'withbody',
            ...config.params
        },
    });

    return data;
}

export const getQuestionById = async (id: ID, config: RequestConfig = {}): Promise<Question> => {
    const {data} = await stackOverflowApi.get<QuestionsResponse>(`questions/${id}`, {
        ...config,
        params: {filter: 'withbody', ...config.params},
    });
    const question = data.items[0];

    if (!question) throw new Error('Question not found');

    return question;
}

export const getQuestionAnswers = async (id: ID, config: RequestConfig = {}): Promise<AnswersResponse> => {
    const {data} = await stackOverflowApi.get<AnswersResponse>(`questions/${id}/answers`, {
        ...config,
        params: {filter: 'withbody', ...config.params},
    });

    return data;
}