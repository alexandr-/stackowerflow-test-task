import type {ID} from '../../shared/types';
import type {StackOverflowResponse} from '../../app';

export type AnswersResponse = StackOverflowResponse<Answer[]>;
export type QuestionsResponse = StackOverflowResponse<Question[]>;

export interface Owner {
    account_id: ID;
    display_name: string;
    link: string;
    profile_image: string;
    reputation: number;
    user_id: ID;
    user_type: string;
}

export interface Question {
    answer_count: number;
    body: string;
    content_license: string;
    creation_date: number;
    is_answered: boolean;
    last_activity_date: number;
    link: string;
    owner: Owner;
    post_state: string;
    question_id: ID;
    score: number;
    tags: string[];
    title: string;
    view_count: number;
}

export interface UserRef {
    account_id: ID;
    display_name: string;
    link: string;
    profile_image: string;
    reputation: number;
    user_id: ID
    user_type: string;
}

export interface Answer {
    comment_id: ID;
    body: string;
    content_license: string;
    creation_date: number;
    edited: boolean;
    owner: Owner;
    post_id: ID;
    reply_to_user?: UserRef;
    score: number;
}