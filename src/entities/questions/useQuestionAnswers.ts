import {useEffect, useState} from 'react';
import {getQuestionAnswers} from './api';
import {validateId} from '../../shared/utils';
import type {ID, Maybe} from '../../shared/types';
import type {Answer} from './types';

export const useQuestionAnswers = (id: ID) => {
    const [answers, setAnswers] = useState<Maybe<Answer[]>>();
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        const abortController = new AbortController();

        setLoading(true);

        getQuestionAnswers(id, {signal: abortController.signal})
            .then(({items}) => setAnswers(items))
            .catch(console.error);

        setLoading(false);

        return () => abortController.abort();
    }, [id]);

    if (!validateId(id)) throw new Error('Invalid question id');

    return {answers, isLoading};
}