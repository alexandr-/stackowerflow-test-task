import {createAsyncThunk} from '@reduxjs/toolkit';
import type {State} from '../../app';
import {selectSearchText} from './selectors';
import {getQuestions as getQuestionsRequest} from './api';

export const thunks = createAsyncThunk(
    'questions/find',
    (_, {getState}) => getQuestionsRequest(selectSearchText(getState() as State))
);