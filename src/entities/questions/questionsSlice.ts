import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {thunks} from './thunks';
import type {QuestionsResponse} from './types';
import type {Maybe} from '../../shared/types';

const questionsSlice = createSlice({
    name: 'questions',
    initialState: {
        searchText: '',
        isLoading: false,
        data: null as Maybe<QuestionsResponse>
    },
    reducers: {
        clear(state) {
            state.data = null;
            state.searchText = '';
            state.isLoading = false;
        },
        changeSearchText(state, {payload}: PayloadAction<string>) {
            state.searchText = payload
        },
    },
    extraReducers(builder) {
        builder.addCase(thunks.pending, (state) => {
            state.isLoading = true;
        });

        builder.addCase(thunks.fulfilled, (state, {payload}) => {
            state.data = payload;
            state.isLoading = false;
        });
    },
});

export const {clear, changeSearchText} = questionsSlice.actions;

export const questionsReducer = questionsSlice.reducer;