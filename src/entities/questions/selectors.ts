import type {State} from '../../app';

export const selectSearchText = (state: State) => state.questions.searchText;

export const selectQuestions = (state: State) => ({
    questions: state.questions.data?.items || [],
    hasData: Boolean(state.questions.data),
    isLoading: state.questions.isLoading,
});