import type {ID, Maybe} from './types';

export const validateId = (id: Maybe<ID>): id is number => {
    return isFinite(Number(id));
}

export const compareId = (id1: ID, id2: ID): boolean => {
    return String(id1) === String(id2);
}