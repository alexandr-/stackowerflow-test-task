import type {State, Dispatch} from '../../app';
import * as redux from 'react-redux';

export const useDispatch: () => Dispatch = redux.useDispatch;

export const useSelector: redux.TypedUseSelectorHook<State> = redux.useSelector