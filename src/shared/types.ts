export type ID = string | number;

export type Maybe<T> = T | null | undefined;