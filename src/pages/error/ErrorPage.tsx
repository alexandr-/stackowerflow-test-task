import type {FC} from 'react';
import {Navigate, useRouteError} from 'react-router-dom';

export const ErrorPage: FC = () => {
    const error = useRouteError();

    alert(JSON.stringify(error, null, 2));

    return <Navigate to='/' replace/>;
}