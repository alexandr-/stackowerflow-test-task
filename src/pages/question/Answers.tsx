import type {FC} from 'react';
import {useQuestionAnswers} from '../../entities/questions';
import {useParams} from 'react-router-dom';
import {InlineLoader} from '../../uikit/loader';
import {CommentsBlock} from './styles';
import {Comment} from '../../components/comment';

export const Answers: FC = () => {
    const {id} = useParams<{id: string}>();
    const {answers, isLoading} = useQuestionAnswers(id!);

    return (
        <CommentsBlock>
            <h3>Answers: {answers?.length}</h3>
            {isLoading
                ? <InlineLoader/>
                : answers?.map(({body, creation_date, owner}) => (
                    <Comment
                        key={creation_date}
                        text={body}
                        date={new Date(creation_date)}
                        author={{
                            name: owner.display_name,
                            url: owner.link
                        }}
                    />
                ))
            }
        </CommentsBlock>
    );
}