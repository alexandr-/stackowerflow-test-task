import styled from 'styled-components';

export const ArticleHeader = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const CommentsBlock = styled.section`
  padding-top: 12px;
  border-top: 1px solid grey;
`;