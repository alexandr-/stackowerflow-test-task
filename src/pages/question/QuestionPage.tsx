import type {FC} from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import {useQuestionById} from '../../entities/questions';
import {Loader} from '../../uikit/loader';
import {Icon} from '../../uikit/icon';
import {Link} from '../../uikit/link';
import {ArticleHeader} from './styles';
import {HtmlView} from '../../components/htmlView';
import {Answers} from './Answers';

export const QuestionPage: FC = () => {
    const navigate = useNavigate();
    const {id} = useParams<{id: string}>();
    const {question, isLoading} = useQuestionById(id!);

    const onNavigateBack = () => {
        navigate('/');
    }

    if (isLoading) return <Loader/>;
    if (!question) return null;

    return (
        <article>
            <ArticleHeader>
                <Icon $size={24} onClick={onNavigateBack}>&#8617;</Icon>
                <Link
                    as='a'
                    target='_blank'
                    rel='noreferrer'
                    href={question.owner.link}
                >
                    {question.owner.display_name}
                </Link>
                <div>
                    {question.tags?.join(', ')}
                </div>
            </ArticleHeader>
            <h2>
                <HtmlView as='span' source={question.title}/>
            </h2>
            <section>
                <HtmlView source={question.body}/>
            </section>
            <Answers/>
        </article>
    );
}