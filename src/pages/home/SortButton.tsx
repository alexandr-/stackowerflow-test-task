import type {FC, ComponentProps, ReactNode} from 'react';
import {Button} from '../../uikit/button';
import {SortDirection} from './types';

interface SortButtonProps extends ComponentProps<typeof Button> {
    active: boolean;
    direction: SortDirection;
    children: ReactNode
}

export const SortButton: FC<SortButtonProps> = ({children, active, direction, ...rest}) => {
    return (
        <Button {...rest}>
            {children} {active && (direction === SortDirection.asc ? <>&#11014;</> : <>&#11015;</>)}
        </Button>
    );
}