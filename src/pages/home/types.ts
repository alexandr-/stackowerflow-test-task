export enum SortDirection {
    asc = 'asc',
    desc = 'desc',
}

export enum Sort {
    ABC = 'ABC',
    Answers = 'Answers',
}