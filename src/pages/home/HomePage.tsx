import {FC, MouseEventHandler, useState} from 'react';
import type {ID, Maybe} from '../../shared/types';
import {useSelector} from '../../shared/hooks';
import {Question, selectQuestions} from '../../entities/questions';
import {QuestionPreview} from '../../components/questions';
import {useNavigate} from 'react-router-dom';
import {SortPanel} from './styles';
import {Sort, SortDirection} from './types';
import {SortButton} from './SortButton';

type SortFunction = (direction: SortDirection, data: Question[]) => Question[];

const sortFunctions: Record<Sort, SortFunction> = {
    ABC: (direction, data) => {
        const sortedData = data.slice().sort((a, b) => Number(a.title > b.title));

        return direction === SortDirection.asc ? sortedData : sortedData.reverse();
    },
    Answers: (direction, data) => {
        const sortedData = data.slice().sort((a, b) => a.answer_count - b.answer_count);

        return direction === SortDirection.asc ? sortedData : sortedData.reverse();
    }
};

export const HomePage: FC = () => {
    const navigate = useNavigate();
    const [sort, setSort] = useState<Maybe<Sort>>();
    const [sortDirection, setSortDirection] = useState<SortDirection>(SortDirection.asc);
    const {questions, hasData} = useSelector(selectQuestions);
    const emptyData = hasData && !questions.length;
    const sortedData = sort ? sortFunctions[sort](sortDirection, questions) : questions;

    const handlePreviewClick = (id: ID): MouseEventHandler => {
        return () => navigate(`/${id}`);
    }

    const handleSelectSort = (newSort: Sort): MouseEventHandler => {
        return () => {
            if (sort !== newSort) setSort(newSort);
            else setSortDirection(direction => direction === SortDirection.asc ? SortDirection.desc : SortDirection.asc);
        }
    }

    if (emptyData) return <h3>Not found</h3>;
    if (!questions.length) return null;

    return (    
        <>
            <SortPanel>
                sort:
                <SortButton
                    children={'A -> Z'}
                    active={sort === Sort.ABC}
                    direction={sortDirection}
                    onClick={handleSelectSort(Sort.ABC)}
                />
                <SortButton
                    children={'Answers'}
                    active={sort === Sort.Answers}
                    direction={sortDirection}
                    onClick={handleSelectSort(Sort.Answers)}
                />
            </SortPanel>
            {sortedData.map((item) =>
                <QuestionPreview
                    key={item.question_id}
                    data={item}
                    onClick={handlePreviewClick(item.question_id)}
                />
            )}
        </>
    );
}