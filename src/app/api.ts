import axios, {AxiosRequestConfig} from 'axios';

export type RequestConfig = AxiosRequestConfig;

export interface StackOverflowResponse<T extends unknown[]> {
    has_more: boolean;
    items: T;
    quota_max: number;
    quota_remaining: number;
}

export const stackOverflowApi = axios.create({
    baseURL: 'https://api.stackexchange.com/',
    params: {
        key: 'Jwp6tuWGAstW2sb76WsCSQ((',
        site: 'stackoverflow'
    }
});