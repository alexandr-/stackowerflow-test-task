export const theme = {
    borderRadius: '8px',
    transition: '.2s ease',
    mixins: {
        container: 'max-width: 900px; margin: 0 auto;',
    }
} as const;

export type Theme = typeof theme;

declare module 'styled-components' {
    export interface DefaultTheme extends Theme {}    
}