import {createBrowserRouter} from 'react-router-dom';
import {PageLayout} from '../components/pageLayout';
import {ErrorPage} from '../pages/error';
import {HomePage} from '../pages/home';
import {QuestionPage} from '../pages/question';

export const router = createBrowserRouter([
    {
        path: '/',
        element: <PageLayout/>,
        errorElement: <ErrorPage/>,
        children: [
            {path: '/', element: <HomePage/>},
            {path: '/:id', element: <QuestionPage/>},
        ]
    },
]);