import {FC, StrictMode} from 'react';
import {ThemeProvider} from 'styled-components';
import {theme} from './theme';
import {Provider} from 'react-redux';
import {store} from './store';
import {RouterProvider} from 'react-router-dom';
import {router} from './router';
import 'normalize.css';

export const App: FC = () => {
    return (
        <StrictMode>
            <ThemeProvider theme={theme}>
                <Provider store={store}>
                    <RouterProvider router={router}/>
                </Provider>
            </ThemeProvider>
        </StrictMode>
    );
}