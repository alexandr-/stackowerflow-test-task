import type {FC, ComponentProps} from 'react';
import {HtmlWrapper} from './styles';

interface HtmlViewProps extends ComponentProps<typeof HtmlWrapper> {
    source: string;
}

export const HtmlView: FC<HtmlViewProps> = ({source, ...rest}) => {
    return <HtmlWrapper {...rest} dangerouslySetInnerHTML={{__html: source}}/>;
}