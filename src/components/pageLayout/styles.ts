import styled from 'styled-components';

export const Content = styled.main`
    ${({theme}) => theme.mixins.container}
    padding: 16px 0;
`;