import type {FC} from 'react';
import {Outlet} from 'react-router-dom';
import {Header} from '../header';
import {Content} from './styles';

export const PageLayout: FC = () => {
    return (
        <>
            <Header/>
            <Content>
                <Outlet/>
            </Content>
        </>
    );
}