import styled from 'styled-components';

export const CommentBlock = styled.div`
  font-size: 14px;

  &:not(:last-child) {
    border-bottom: 1px solid grey;
  }
`;

export const CommentInfo = styled.div`
  display: flex;
  justify-content: space-between;
`;