import type {FC} from 'react';
import {HtmlView} from '../htmlView';
import {CommentBlock, CommentInfo} from './styles';
import {Link} from '../../uikit/link';

interface CommentProps {
    text: string;
    date: Date;
    author: {name: string; url: string};
}

export const Comment: FC<CommentProps>  = ({text, date, author}) => {
    return (
        <CommentBlock>
            <HtmlView source={text}/>
            <CommentInfo>
                <span>{date.toLocaleString()}</span>
                <Link
                    as='a'
                    target='_blank'
                    rel='noreferrer'
                    href={author.url}
                >
                    {author.name}
                </Link>
            </CommentInfo>
        </CommentBlock>
    );
}