import styled from 'styled-components';

export const HeaderWrap = styled.header`
  position: sticky;
  top: 0;
  padding: 8px;
  background-color: #ffffff;
  box-shadow: 0 1px 2px hsla(0,0%,0%,0.05), 0 1px 4px hsla(0, 0%, 0%, 0.05), 0 2px 8px hsla(0, 0%, 0%, 0.05);
`;

export const Content = styled.div`
  ${({theme}) => theme.mixins.container}
`;

export const SearchForm = styled.form`
  max-width: 500px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  gap: 15px;
`;