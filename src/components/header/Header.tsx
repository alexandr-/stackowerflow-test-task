import {FC, ChangeEvent, FormEvent} from 'react';
import {HeaderWrap, Content, SearchForm} from './styles';
import {Input} from '../../uikit/input';
import {useDispatch, useSelector} from '../../shared/hooks';
import {thunks, selectSearchText, changeSearchText, clear, selectQuestions} from '../../entities/questions';
import {Button} from '../../uikit/button';
import {Icon} from '../../uikit/icon';
import {Spinner} from '../../uikit/spinner';

export const Header: FC = () => {
    const {isLoading} = useSelector(selectQuestions);
    const text = useSelector(selectSearchText);
    const dispatch = useDispatch();

    const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        dispatch(changeSearchText(event.target.value));
    }

    const onSubmit = (event: FormEvent) => {
        event.preventDefault();

        if (!text.trim()) return;

        dispatch(thunks());
    }

    const onClear = () => {
        dispatch(clear());
    }

    return (
        <HeaderWrap>
            <Content>
                <SearchForm onSubmit={onSubmit}>
                    <Input
                        value={text}
                        onChange={onInputChange}
                        placeholder='Поиск...'
                        startIcon={<Icon>&#128269;</Icon>}
                        endIcon={(
                            isLoading
                                ? <Spinner $size={14}/>
                                : <Icon hidden={!text} onClick={onClear}>
                                    &times;
                                </Icon>
                        )}
                    />
                    <Button>
                        Найти
                    </Button>
                </SearchForm>
            </Content>
        </HeaderWrap>
    );
}