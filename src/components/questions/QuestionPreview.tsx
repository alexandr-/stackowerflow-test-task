import type {FC, ComponentProps} from 'react';
import type {Question} from '../../entities/questions';
import {Preview, PreviewTitle, PreviewFooter, PreviewAnswers} from './styles';
import {Link} from '../../uikit/link';
import {HtmlView} from '../htmlView';

interface QuestionPreviewProps extends ComponentProps<typeof Preview> {
    data: Question;
}

export const QuestionPreview: FC<QuestionPreviewProps> = ({data, ...rest}) => {
    return (
        <Preview {...rest}>
            <PreviewTitle>
                <HtmlView as='span' source={data.title}/>
            </PreviewTitle>
            <PreviewFooter>
                <div>
                    <Link as='a' target='_blank' rel='noreferrer' href={data.owner.link}>
                        {data.owner?.display_name}
                    </Link>
                    <PreviewAnswers>ответы: {data.answer_count}</PreviewAnswers>
                </div>
                <div>{data.tags.join(', ')}</div>
            </PreviewFooter>
        </Preview>
    );
}