import styled from 'styled-components';

export const Preview = styled.div`
  padding: 12px;
  cursor: pointer;
  transition: box-shadow ${({theme}) => theme.transition};
  
  &:not(:last-child) {
    margin-bottom: 16px;
  }

  &:hover {
    box-shadow: 0 1px 2px hsla(0,0%,0%,0.05), 0 1px 4px hsla(0, 0%, 0%, 0.05), 0 2px 8px hsla(0, 0%, 0%, 0.05);
  }
`;

export const PreviewTitle = styled.h3`
  margin: 0 0 5px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const PreviewFooter = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const PreviewAnswers = styled.small`
  display: inline-block;
  margin-left: 12px;
  color: #444;
`;